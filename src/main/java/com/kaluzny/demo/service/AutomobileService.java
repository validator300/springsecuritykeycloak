package com.kaluzny.demo.service;


import com.kaluzny.demo.domain.Automobile;
import com.kaluzny.demo.domain.AutomobileRepository;
import jakarta.jms.Topic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
@Slf4j
@Service
public class AutomobileService {
    private final AutomobileRepository repository;
    private final JmsTemplate jmsTemplate;
    @Autowired
    public AutomobileService(AutomobileRepository repository, JmsTemplate jmsTemplate) {
        this.repository = repository;
        this.jmsTemplate = jmsTemplate;
    }
    public ResponseEntity<List<Automobile>> getAutomobilesByBrandAndColor(Automobile automobile) {
        try {
            List<Automobile> automobiles =
                    repository.findByNameAndColor(automobile.getName(), automobile.getColor());

            // Отправить список автомобилей с нужным цветом в JMS
            Topic autoTopic = Objects.requireNonNull(jmsTemplate
                    .getConnectionFactory()).createConnection().createSession()
                    .createTopic("AutoTopic_red");
            log.info("\u001B[32m" + "Sending Automobiles with color " + automobile.getColor() + ": "
                    + automobiles.stream()
                    .map(Automobile::getId)
                    .toList() + "\u001B[0m");
            jmsTemplate.convertAndSend(autoTopic, automobiles);

            return new ResponseEntity<>(automobiles, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
