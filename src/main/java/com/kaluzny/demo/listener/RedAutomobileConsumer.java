package com.kaluzny.demo.listener;

import com.kaluzny.demo.domain.Automobile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.IntStream;

@Slf4j
@Component
public class RedAutomobileConsumer {
    @JmsListener(destination = "AutoTopic_red", containerFactory = "automobileJmsContFactory")
    public void getRedAutomobiles(List<Automobile> automobiles) {
        IntStream.range(0, automobiles.size())
                .forEach(i -> log.info("\u001B[31m" + "Red Automobile " + (i + 1) + ": " + automobiles.get(i) + "\u001B[0m"));
    }
}

