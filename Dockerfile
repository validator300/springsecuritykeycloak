FROM openjdk:17

WORKDIR /app

COPY target/spring-boot-keycloak-docker-postgres.jar spring-boot-keycloak-docker-postgres.jar

ENTRYPOINT ["java", "-jar", "spring-boot-keycloak-docker-postgres.jar"]